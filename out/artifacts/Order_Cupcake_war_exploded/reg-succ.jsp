<%--
  Created by IntelliJ IDEA.
  User: moisesemiliocuevamolina
  Date: 7/21/20
  Time: 07:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<html>
<head>
    <meta charset="ISO-8859-1">
    <title>Success registration</title>
</head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles%20/login.css" media="screen"/>
<link href='https://fonts.googleapis.com/css?family=Sofia' rel='stylesheet'>
<body>
<section class="header">
    <h1 class="logoName" > Sweet and Cake </h1>
    <div class="header-right">
        <a class="active" href="shopping.jsp">Order</a>
        <a href="index.jsp">About us</a>
        <a href="index.jsp">Contact</a>
        <br>
    </div>
</section>

<div class="text1">
    <div align="center">
        <h1>Successfully registered!</h1>
    </div>
    <button class="btn"type="button" onclick="location.href='shopping.jsp'">Continue</button>
</div>
<footer class="full-footer-1">
    <div class="cont full-footer-cont-1">
        <ul class="nav footer-nav-1">
            <li>
                <a href="https://www.youtube.com" target="_blank">
                    <img src="image/socialMedia/yot.png">
                </a>
            </li>
            <li>
                <a href="https://www.instegram.com" target="_blank">
                    <img src="image/socialMedia/ins.jpg">
                </a>
            </li>
            <li>
                <a href="https://www.facebook.com" target="_blank">
                    <img src="image/socialMedia/facb.png">
                </a>
            </li>
        </ul>
    </div>
</footer>
</body>
</html>