if (document.readyState == 'loading') {
    document.addEventListener('DOMContentLoaded', ready)
} else {
    ready()
}
function ready() {
    let button;
    let i;
    let removeBasketItemButtons = document.getElementsByClassName('btn-danger');
    for (i = 0; i < removeBasketItemButtons.length; i++) {
        button = removeBasketItemButtons[i];
        button.addEventListener('click', removeBasketItem)
    }

    const quantityInputs = document.getElementsByClassName('basket-quantity-input');
    for (i = 0; i < quantityInputs.length; i++) {
        const input = quantityInputs[i];
        input.addEventListener('change', quantityChanged)
    }

    const addToBasketButtons = document.getElementsByClassName('item-button');
    for (i = 0; i < addToBasketButtons.length; i++) {
        button = addToBasketButtons[i];
        button.addEventListener('click', addToBasketClicked)
    }

    document.getElementsByClassName('btn-purchase')[0].addEventListener('click', purchaseClicked)
}

function purchaseClicked() {

    const basketItems = document.getElementsByClassName('basket-items')[0];
    while (basketItems.hasChildNodes()) {
        basketItems.removeChild(basketItems.firstChild)
    }
    updateBasketTotal()
}

function removeBasketItem(event) {
    const buttonClicked = event.target;
    buttonClicked.parentElement.parentElement.remove()
    updateBasketTotal()
}

function quantityChanged(event) {
    const input = event.target;
    if (isNaN(input.value) || input.value <= 0) {
        input.value = 1
    }
    updateBasketTotal()
}

function addToBasketClicked(event) {
    const button = event.target;
    const shopItem = button.parentElement.parentElement;
    const title = shopItem.getElementsByClassName('item-title')[0].innerText;
    const price = shopItem.getElementsByClassName('item-price')[0].innerText;
    const imageSrc = shopItem.getElementsByClassName('item-image')[0].src;
    addItemToBasket(title, price, imageSrc)
    updateBasketTotal()
}

function addItemToBasket(title, price, imageSrc) {
    const basketRow = document.createElement('div');
    basketRow.classList.add('basket-row')
    const basketItems = document.getElementsByClassName('basket-items')[0];
    const basketItemNames = basketItems.getElementsByClassName('basket-item-title');
    for (let i = 0; i < basketItemNames.length; i++) {
        if (basketItemNames[i].innerText === title) {
            alert('This item is already added to the basket')
            return
        }
    }
    basketRow.innerHTML = `
        <div class="basket-item basket-column">
            <img class="basket-item-image" src="${imageSrc}" width="100" height="100" alt="100">
            <span class="basket-item-title">${title}</span>
        </div>
        <span class="basket-price basket-column">${price}</span>
        <div class="basket-quantity basket-column">
            <input class="basket-quantity-input" type="number" value="1">
            <button class="btn btn-danger" type="button">REMOVE</button>
        </div>`
    basketItems.append(basketRow)
    basketRow.getElementsByClassName('btn-danger')[0].addEventListener('click', removeBasketItem)
    basketRow.getElementsByClassName('basket-quantity-input')[0].addEventListener('change', quantityChanged)
}

function updateBasketTotal() {
    const basketItemContainer = document.getElementsByClassName('basket-items')[0];
    const basketRows = basketItemContainer.getElementsByClassName('basket-row');
    let total = 0;
    for (let i = 0; i < basketRows.length; i++) {
        const basketRow = basketRows[i];
        const priceElement = basketRow.getElementsByClassName('basket-price')[0];
        const quantityElement = basketRow.getElementsByClassName('basket-quantity-input')[0];
        const price = parseFloat(priceElement.innerText.replace('$', ''));
        const quantity = quantityElement.value;
        total = total + (price * quantity)
    }
    total = Math.round(total * 100) / 100
    document.getElementsByClassName('basket-total-price')[0].innerText = '$' + total
}