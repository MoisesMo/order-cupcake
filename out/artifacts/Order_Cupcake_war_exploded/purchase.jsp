<%--
  Created by IntelliJ IDEA.
  User: moisesemiliocuevamolina
  Date: 7/22/20
  Time: 16:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Payment Checkout Form</title>
    <link href='https://fonts.googleapis.com/css?family=Sofia' rel='stylesheet'>
    <link rel="stylesheet" href="styles%20/purchase.css">

</head>
<body>
<section class="header">
    <h1 class="logoName" > Sweet and Cake </h1>
    <div class="header-right">
        <a class="active" href="shopping.jsp">Order</a>
        <a href="index.jsp">About us</a>
        <a href="index.jsp">Contact</a>
        <br>
    </div>

</section>

<div class="wrapper">
    <div class="payment">
        <div class="payment-logo">
            <p>p</p>
        </div>


        <h2>Payment</h2>
        <div class="form">
            <div class="pay sp icon-relative">
                <label class="label">Card holder:</label>
                <input type="text" class="input" placeholder="Full name">
                <i class="fas fa-user"></i>
            </div>
            <div class="pay sp icon-relative">
                <label class="cardNumber">Card number:</label>
                <input type="text" class="input" data-mask="0000 0000 0000 0000" placeholder="Card Number">
                <i class="far fa-credit-card"></i>
            </div>
            <div class="pay-number sp">
                <div class="pay-item icon-relative">
                    <label class="label">Expiry date:</label>
                    <input type="text" name="expiry-data" class="input" data-mask="00 / 00"  placeholder="00 / 00">
                    <i class="far fa-calendar-alt"></i>
                </div>
                <div class="pay-item icon-relative">
                    <label class="label">CVC:</label>
                    <input type="text" class="input" data-mask="000" placeholder="000">
                    <i class="fas fa-lock"></i>
                </div>
            </div>

            <div class="btn">
                Pay
            </div>

        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
<footer class="full-footer-1">
    <div class="cont full-footer-cont-1">
        <ul class="nav footer-nav-1">
            <li>
                <a href="https://www.youtube.com" target="_blank">
                    <img src="image/socialMedia/yot.png">
                </a>
            </li>
            <li>
                <a href="https://www.instegram.com" target="_blank">
                    <img src="image/socialMedia/ins.jpg">
                </a>
            </li>
            <li>
                <a href="https://www.facebook.com" target="_blank">
                    <img src="image/socialMedia/facb.png">
                </a>
            </li>
        </ul>
    </div>
</footer>
</body>
</html>