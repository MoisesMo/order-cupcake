<%--
  Created by IntelliJ IDEA.
  User: moisesemiliocuevamolina
  Date: 16/07/2020
  Time: 23:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Login or SignUp</title>
</head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles%20/log-Reg.jsp.css" media="screen"/>
<link href='https://fonts.googleapis.com/css?family=Sofia' rel='stylesheet'>
<body>

<section class="header">
    <h1 class="logoName" > Sweet and Cake </h1>
    <div class="header-right">
        <a class="active" href="log-Reg.jsp">Order</a>
        <a href="index.jsp">About us</a>
        <a href="index.jsp">Contact</a>
    </div>
</section>
<div class="text">
    <h1>My Account</h1>
    <h3>If you have an account please press Log in</h3>
    <h3>If you have no account please press Sign Up</h3>
</div>

<section class="log-btn">
    <div class="log-btn">
        <button class="log-reg-btn1"type="button" onclick="location.href='login.jsp'">Log in</button>
        <button class="log-reg-btn2"type="button" onclick="location.href='register.jsp'">Sign Up</button>
    </div>
</section>

<footer class="full-footer">
    <div class="cont full-footer-cont">
        <ul class="nav footer-nav">
            <li>
                <a href="https://www.youtube.com" target="_blank">
                    <img src="image/socialMedia/yot.png">
                </a>
            </li>
            <li>
                <a href="https://www.instegram.com" target="_blank">
                    <img src="image/socialMedia/ins.jpg">
                </a>
            </li>
            <li>
                <a href="https://www.facebook.com" target="_blank">
                    <img src="image/socialMedia/facb.png">
                </a>
            </li>
        </ul>
    </div>
</footer>

</body>
</html>
