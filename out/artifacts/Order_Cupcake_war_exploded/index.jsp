<%--
  Created by IntelliJ IDEA.
  User: moisesemiliocuevamolina
  Date: 16/07/2020
  Time: 10:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Principle Page</title>
</head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles%20/index.css" media="screen"/>
<link href='https://fonts.googleapis.com/css?family=Sofia' rel='stylesheet'>
<body>

<section class="header">
  <h1 class="logoName" > Sweet and Cake </h1>
  <div class="header-right">
    <a class="active" href="log-Reg.jsp">Order</a>
    <a href="index.jsp">About us</a>
    <a href="index.jsp">Contact</a>
  </div>
</section>

<br>
<div class="slideshow-container">

  <div class="mySlides fade">
    <img src="image/index/cupcakeL1.jpeg"  style="width:100%">
    <button class="btn"type="button" onclick="location.href='log-Reg.jsp'">Order Now</button>
  </div>

  <div class="mySlides fade">
    <img src="image/index/cupcakeL2.jpeg" style="width:100%" >
    <button class="btn"type="button" onclick="location.href='log-Reg.jsp'">Order Now</button>
  </div>

  <div class="mySlides fade">
    <img src="image/index/cupcakeL3.jpeg" style="width:100%">
    <button class="btn" type="button" onclick="location.href='log-Reg.jsp'">Order Now</button>
  </div>

</div>
<br>

<div style="text-align:center">
  <span class="dot"></span>
  <span class="dot"></span>
  <span class="dot"></span>
</div>

<script>
  let slideIndex = 0;
  showSlides();

  function showSlides() {
    let i;
    let slides = document.getElementsByClassName("mySlides");
    let dots = document.getElementsByClassName("dot");
    for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
    }
    slideIndex++;
    if (slideIndex > slides.length) {slideIndex = 1}
    for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex-1].style.display = "block";
    dots[slideIndex-1].className += " active";
    setTimeout(showSlides, 4000);
  }
</script>

<footer class="full-footer">
  <div class="full-footer-cont">
    <ul class="nav footer-nav">
      <li>
        <a href="https://www.youtube.com" target="_blank">
          <img src="image/socialMedia/yot.png">
        </a>
      </li>
      <li>
        <a href="https://www.instagram.com/?hl=it" target="_blank">
          <img src="image/socialMedia/ins.jpg">
        </a>
      </li>
      <li>
        <a href="https://www.facebook.com" target="_blank">
          <img src="image/socialMedia/facb.png">
        </a>
      </li>
    </ul>
  </div>
</footer>
</body>
