<%--
  Created by IntelliJ IDEA.
  User: moisesemiliocuevamolina
  Date: 7/21/20
  Time: 07:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<html>
<head>
    <meta charset="UTF-8">
    <title>Principle Page</title>
</head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles%20/register.css" media="screen"/>
<link href='https://fonts.googleapis.com/css?family=Sofia' rel='stylesheet'>
<body>

<section class="header">
    <h1 class="logoName" > Sweet and Cake </h1>
    <div class="header-right">
        <a class="active" href="log-Reg.jsp">Order</a>
        <a href="index.jsp">About us</a>
        <a href="index.jsp">Contact</a>
    </div>
</section>

<div class="text2" align="center">
    <div style="color: tomato;">
        <h2>Account Registration </h2>
    </div>
    <hr>
    <div>

        <div>

            <h2>Personal information</h2>
            <div>

                <form action="<%=request.getContextPath()%>/register" method="post">

                    <div>
                        <label for="firstName">First Name:</label> <input type="text"
                                                                          class="form-control" id="firstName" placeholder="First Name"
                                                                          name="firstName" required>
                    </div>

                    <div>
                        <label for="middleName">Middle Name:</label> <input type="text"
                                                                            class="form-control" id="middleName" placeholder="Middle Name"
                                                                            name="middleName" required>
                    </div>

                    <div>
                        <label for="lastName">Last Name:</label> <input type="text"
                                                                        class="form-control" id="lastName" placeholder="Last Name"
                                                                        name="lastName" required>
                    </div>

                    <div>
                        <label for="birthday">Birthday Date:</label> <input type="text"
                                                                            class="form-control" id="birthday" placeholder="Birthday Date"
                                                                            name="birthday" required>
                    </div>

                    <div>
                        <label for="mobile">Mobile Phone:</label> <input type="text"
                                                                         class="form-control" id="mobile" placeholder="Mobile Phone"
                                                                         name="mobile" required>
                    </div>

                    <div>
                        <label for="username">User Name:</label> <input type="text"
                                                                        class="form-control" id="username" placeholder="User Name"
                                                                        name="username" required>
                    </div>

                    <div>
                        <label for="password">Password:</label> <input type="password"
                                                                       class="form-control" id="password" placeholder="Password"
                                                                       name="password" required>
                    </div>

                    <button type="submit" class="btn">Submit</button>

                </form>
            </div>
        </div>
    </div>
</div>

<footer class="full-footer-2">
    <div class="cont full-footer-cont-2">
        <ul class="nav footer-nav-2">
            <li>
                <a href="https://www.youtube.com" target="_blank">
                    <img src="image/socialMedia/yot.png">
                </a>
            </li>
            <li>
                <a href="https://www.instegram.com" target="_blank">
                    <img src="image/socialMedia/ins.jpg">
                </a>
            </li>
            <li>
                <a href="https://www.facebook.com" target="_blank">
                    <img src="image/socialMedia/facb.png">
                </a>
            </li>
        </ul>
    </div>
</footer>
</body>
</html>

