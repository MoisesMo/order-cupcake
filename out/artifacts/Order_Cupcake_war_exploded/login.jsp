<%--
  Created by IntelliJ IDEA.
  User: moisesemiliocuevamolina
  Date: 16/07/2020
  Time: 22:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<html>
<head>
    <meta charset="UTF-8">
    <title>Principle Page</title>
</head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles%20/login.css" media="screen"/>
<link href='https://fonts.googleapis.com/css?family=Sofia' rel='stylesheet'>
<body>

<section class="header">
    <h1 class="logoName" > Sweet and Cake </h1>
    <div class="header-right">
        <a class="active" href="log-Reg.jsp">Order</a>
        <a href="index.jsp">About us</a>
        <a href="index.jsp">Contact</a>
    </div>
</section>
<div class="text1" align="center">
    <h1>Login to Order</h1>
    <form action="<%=request.getContextPath()%>/login" method="post">
        <table style="with: 100%">
            <tr>
                <td>UserName</td>
                <td><input type="text" name="username" /></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><input type="password" name="password" /></td>
            </tr>

        </table>
        <input class="btn" type="submit" value="Submit" />
    </form>
</div>

<footer class="full-footer-1">
    <div class="cont full-footer-cont-1">
        <ul class="nav footer-nav-1">
            <li>
                <a href="https://www.youtube.com" target="_blank">
                    <img src="image/socialMedia/yot.png">
                </a>
            </li>
            <li>
                <a href="https://www.instegram.com" target="_blank">
                    <img src="image/socialMedia/ins.jpg">
                </a>
            </li>
            <li>
                <a href="https://www.facebook.com" target="_blank">
                    <img src="image/socialMedia/facb.png">
                </a>
            </li>
        </ul>
    </div>
</footer>

</body>
</html>
