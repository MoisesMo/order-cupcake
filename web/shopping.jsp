<%--
Created by IntelliJ IDEA.
User: moisesemiliocuevamolina
Date: 16/07/2020
Time: 10:34
To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Order cupcake</title>
</head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles%20/shopping.css" media="screen"/>
<link href='https://fonts.googleapis.com/css?family=Sofia' rel='stylesheet'>
<script src="js/Funtion-Shopping.js" async></script>
<body>

<section class="header">
    <h1 class="logoName" > Sweet and Cake </h1>
    <div class="header-right">
        <a class="active" href="shopping.jsp">Order</a>
        <a href="index.jsp">About us</a>
        <a href="index.jsp">Contact</a>
        <br>
    </div>

</section>

<section class="cont cont-sect">
    <div class="shop-items">
        <div class="shop-item">
            <span class="item-title">Freakfetti</span>
            <img class="item-image" src="image/Order/cup1-1.jpg">
            <div class="item-details">
                <span class="item-price">$4.50</span>
                <button class="btn item-button" type="button">ADD TO BASKET</button>
            </div>
        </div>
        <div class="shop-item">
            <span class="item-title">Mionight Galaxy</span>
            <img class="item-image" src="image/Order/cupcake2-2.jpg">
            <div class="item-details">
                <span class="item-price">$5.50</span>
                <button class="btn item-button"type="button">ADD TO BASKET</button>
            </div>
        </div>
        <div class="shop-item">
            <span class="item-title">Black Forest Waffle</span>
            <img class="item-image" src="image/Order/cupcake3-3.jpg">
            <div class="item-details">
                <span class="item-price">$4.00</span>
                <button class="btn item-button" type="button">ADD TO BASKET</button>
            </div>
        </div>
        <div class="shop-item">
            <span class="item-title">Bubble Pop Electic </span>
            <img class="item-image" src="image/Order/cup3-3.jpeg">
            <div class="item-details">
                <span class="item-price">$6.00</span>
                <button class="btn item-button" type="button">ADD TO BASKET</button>
            </div>
        </div>
    </div>
</section>

<section class="cont cont-sect">
    <h2 class="quantity">Quantity</h2>
    <div class="basket-row">
        <span class="basket-item basket-header basket-column">ITEM</span>
        <span class="basket-price basket-header basket-column">PRICE</span>
        <span class="basket-quantity basket-header basket-column">QUANTITY</span>
    </div>
    <div class="basket-items">
    </div>
    <div class="basket-total">
        <strong class="basket-total-title">Total</strong>
        <span class="basket-total-price">$0</span>
    </div>
    <button class="btn btn-purchase"  type="button"  onclick="location.href='purchase.jsp'">PURCHASE</button>

</section>
<footer class="full-footer">
    <div class="cont full-footer-cont">
        <ul class="nav footer-nav">
            <li>
                <a href="https://www.youtube.com" target="_blank">
                    <img src="image/socialMedia/yot.png">
                </a>
            </li>
            <li>
                <a href="https://www.instegram.com" target="_blank">
                    <img src="image/socialMedia/ins.jpg">
                </a>
            </li>
            <li>
                <a href="https://www.facebook.com" target="_blank">
                    <img src="image/socialMedia/facb.png">
                </a>
            </li>
        </ul>
    </div>
</footer>
</body>
</html>